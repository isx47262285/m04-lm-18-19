#!/usr/local/bin/python
#-*- coding: utf-8-*-
# nombre: roberto altamirano martinez
# isx47262285
# descripcion:
#     exercicis de consultas xml 
# synopsis:
# 		
# Ejercicios de XPath
##########################################################################
from lxml import etree

arbol = etree.parse ("./institut.xml")

# comprovacio de funcionament 

#print arbol.getroot().tag
'''
# afegeix un profesor amb: codi 8 marita i carrec profesora.

professors = arbol.find("professors")

print profesors

Professor = etree.Element("Profesor")

professors.append(profesor)


arbol.write("institut.xml" , pretty_print = True)
#elementoHijo.SetAttribute("type","modeloAAA");
'''


#2 el modul m04 pasa a tener 8 horas (yes)


'''
horarios = arbol.findall("Horaris/Horari[@modul='m04']")

horarios[0].set("durada", "8")

arbol.write("institut.xml" , pretty_print = True)

'''

#3 esborrar al profesor rafael 

'''
profesors=arbol.findall("Professors/Professor")

print profesors

profesors.remove(profesors.find("Professor[Nom='Rafel']"))

arbol.write("institut.xml" , pretty_print = True)
'''


#4 profesors que hacen clase el martes (yes)
'''
profesors = arbol.xpath("//Professors/Professor")

codigos_id = arbol.xpath("//Horaris/Horari[Classe[@dia='Dimarts']]")
lista_codigos =[]
for codigo in codigos_id:
	codigo_profesor = codigo.attrib['professor']
	if codigo_profesor not in lista_codigos:
		lista_codigos.append(codigo_profesor)
print lista_codigos
	

for codigo in lista_codigos:
	
	print arbol.findall("Professors/Professor[@id='%s']/Nom" % codigo)[0].text 
'''
#5 aules que estan ocupades a les 9 (yes)
'''
horaris = arbol.xpath("//Horaris/Horari[Classe[@hora='9']]")
lista_moduls =[]

for modul in horaris:
	
	lista_moduls.append(modul.attrib['modul'])

print lista_moduls # lista vacia no abra ninguna clase abierta alas 9


for aula in lista_moduls:
	
	print arbol.xpath("//Moduls/Modul[@id='%s']/Aula" % aula)
	

'''

#6 asignaturas impartidas per jordi (yes)
'''

codi_jordi = arbol.xpath("//Professor[Nom='Jordi']")

print codi_jordi[0].attrib['id']

asignaturas = arbol.xpath("//Horaris/Horari[@professor='%s']" % codi_jordi[0].attrib['id'])

for modulo  in asignaturas:
	
	print modulo.attrib['modul']
'''


